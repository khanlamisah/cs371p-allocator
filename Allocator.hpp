// -------------
// Allocator.hpp
// -------------

#ifndef Allocator_hpp
#define Allocator_hpp

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument


using namespace std;

// ------------
// My_Allocator
// ------------

template <typename T, std::size_t N>
class My_Allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const My_Allocator&, const My_Allocator&) { // this is correct
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const My_Allocator& lhs, const My_Allocator& rhs) { // this is correct
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using value_type      = T;

    using size_type       = std::size_t;
    using difference_type = std::ptrdiff_t;

    using pointer         =       value_type*;
    using const_pointer   = const value_type*;

    using reference       =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) { // fix!
            // <your code>
            return (&lhs._r[lhs._i] == &rhs._r[rhs._i]);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) { // this is correct
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        My_Allocator& _r;
        std::size_t   _i;

    public:
        // -----------
        // constructor
        // -----------

        iterator (My_Allocator& r, size_type i) :
            _r (r),
            _i (i)
        {}

        // ----------
        // operator *
        // ----------

        /**
         * beginning sentinel of the block
         */
        int& operator * () const { // fix!
            return _r[_i];
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () { // fix!
            _i += abs(**this) + 2 * sizeof(int);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) { // this is correct
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () { // fix!
            _i -= sizeof(int);
            _i -= abs(**this) + sizeof(int);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) { // this is correct
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) { // fix!
            // <your code>
            return (&lhs._r[lhs._i] == &rhs._r[rhs._i]);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) { // this is correct
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const My_Allocator& _r;
        std::size_t         _i;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const My_Allocator& r, size_type i) :
            _r (r),
            _i (i)
        {}

        // ----------
        // operator *
        // ----------

        // beginning sentinel of the block
        const int& operator * () const { // fix!
            // <your code>
            return _r[_i];
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () { // fix!
            // <your code>
            _i += abs(**this) + 2 * sizeof(int);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) { // this is correct

            const_iterator tmp = *this;
            ++*this;
            return tmp;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () { // fix!
            // <your code>
            _i -= sizeof(int);
            _i -= abs(**this) + sizeof(int);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) { // this is correct
            const_iterator tmp = *this;
            --*this;
            return tmp;
        }
    };

private:
    // ----
    // data
    // ----
    char a[N]; // array of bytes

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Checks if heap is in a valid state: beginning sentinels and ending
     * sentinels have same value and sentinels are accurate
     */
    bool valid () const {
        // <you must use allocator's iterators>

        const_iterator b = begin();
        const_iterator e = end();

        int index = 0;

        while (b != e) {

            // checks both conditions for validity
            // if sentinel size is not accurate, then rhs of bool expr
            // will not equal lhs of bool expr
            if ((*this)[index] != (*this)[abs((*this)[index]) + sizeof(int)])
                return false;
            ++b;
        }

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a std::bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    My_Allocator () {
        if (N < (sizeof(T) + (2 * sizeof(int))))
            throw std::bad_alloc();
        (*this)[0]   = N-8;
        (*this)[N-4] = N-8;
        assert(valid());
    }

    My_Allocator             (const My_Allocator&) = default;
    ~My_Allocator            ()                    = default;
    My_Allocator& operator = (const My_Allocator&) = default;


    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a std::bad_alloc exception, if there isn't an acceptable free block
     */
    pointer allocate (size_type s) { // fix!

        assert(s != 0);

        iterator b = begin();
        iterator e = end();

        bool found = false;
        int index = 0;
        int n_bytes = s * sizeof(T);

        while (b != e) {
            int curr_block_size = *b;

            // free block
            if (curr_block_size > 0) {
                // block is big enough for user but not big enough to split
                if (curr_block_size >=  n_bytes && curr_block_size < n_bytes + 2 * (int) sizeof(int) + sizeof(T)) {

                    (*this)[index] = -curr_block_size;
                    (*this)[index + sizeof(int) + curr_block_size] = -curr_block_size;

                    found = true;
                    break;
                }
                // block is big enough to split
                else if (curr_block_size >= n_bytes + 2 * (int) sizeof(int) + sizeof(T)) {

                    int free_block_size = curr_block_size - n_bytes - 2 * sizeof(int);
                    (*this)[index] = -n_bytes;
                    (*this)[index + sizeof(int) + n_bytes] = -n_bytes;
                    (*this)[index + 2 * sizeof(int) + n_bytes] = free_block_size;
                    (*this)[index + 3 * sizeof(int) + n_bytes + free_block_size] = free_block_size;

                    found = true;
                    break;
                }
            }
            ++b;
            index += abs(curr_block_size) + 2 * sizeof(int);
        }

        if (!found)
            throw std::bad_alloc();

        assert(valid());
        // return T* pointing to data of block
        return (T*) (a + index + sizeof(int));
    }


    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {// this is correct and exempt
        new (p) T(v);                               // from the prohibition of new
        assert(valid());
    }


    // ----------
    // find_busy_block
    // ----------

    /**
     * Finds the nth busy block in the heap and returns a pointer pointing
     * to the data of the block
     * If there is no nth busy block, return null pointer
     */

    pointer find_busy_block (int n) {

        iterator b = begin();
        iterator e = end();

        n = abs(n);
        int index = 0;

        while (b != e) {

            // if busy, decrement n
            if (*b < 0)
                --n;
            // if n == 0, we found the nth busy block
            if (n == 0)
                break;

            index += abs(*b) + 2 * sizeof(int);

            ++b;

        }
        if (index == N)
            return 0;
        return (T*) (a + index + sizeof(int));
    }


    // ----------
    // coalesce
    // ----------

    /**
     * Coalesces two free blocks. iterator i is pointing to the first block
     * and index is first block's index in the heap
     */

    void coalesce (iterator i, size_type index) {
        int prev_block_size = *i;
        int curr_block_size = *++i;
        int new_block_size = prev_block_size + curr_block_size + 2 * sizeof(int);

        (*this)[index] = new_block_size;
        (*this)[index + new_block_size + sizeof(int)] = new_block_size;

        assert(valid());
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type s = size_t()) {
        // invalid pointer
        if ((char*) p < a || (char*) p >= (a + N))
            throw std::invalid_argument("Pointer out of bounds.");

        // find index of pointer's beginning sentinel
        char* cp = (char*) p - sizeof(int);
        size_t index = cp - a;

        // find size of current block
        iterator d = iterator(*this, index);
        int curr_block_size = -*d;

        assert(curr_block_size > 0);

        // free block
        (*this)[index] = curr_block_size;
        (*this)[index + sizeof(int) + curr_block_size] = curr_block_size;

        // coalescing
        bool prev_coalesce = false;
        bool is_begin = d == begin();

        // if current block is not beginning and previous block is free, coalesce
        // note: if current block is not beginning, d is decremented!!!!
        if (!is_begin && *--d > 0) {
            prev_coalesce = true;
            // first block in coalesce is previous block so update index
            index -= 2 * sizeof(int) + *d;
            coalesce(d, index);
        }

        // if current block is not beginning and previous block is busy,
        // then d is currently pointing to previous block. d needs to point
        // to current block. fix by incrementing.
        if (!prev_coalesce && !is_begin)
            ++d;

        // if current block is not last block and next block is free, coalesce
        if (d != --end() && *++d > 0) {
            // d is incremented!!! first block in coalesce is current block
            // so decrement d
            coalesce(--d, index);
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) { // this is correct
        p->~T();
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) { // this is correct
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const { // this is correct
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () { // this is correct
        return iterator(*this, 0);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const { // this is correct
        return const_iterator(*this, 0);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () { // this is correct
        return iterator(*this, N);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const { // this is correct
        return const_iterator(*this, N);
    }
};

#endif // Allocator_hpp

