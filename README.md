# CS371p: Object-Oriented Programming Allocator Repo

* Name: Lamisah Khan

* EID: lak2499

* GitLab ID: khanlamisah

* HackerRank ID: lamisah_khan

* Git SHA: f82e66b0a57a2cbdb1d41b92d6e9735466d02032

* GitLab Pipelines: https://gitlab.com/khanlamisah/cs371p-allocator/-/jobs/6336981640

* Estimated completion time: 8

* Actual completion time: 48

* Comments: Allocator.ctd.txt checks checktestdata. Allocator.gen.ctd.txt generates input for test.
