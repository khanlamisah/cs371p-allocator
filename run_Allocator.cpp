// ----------------
// RunAllocator.cpp
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl
#include <iterator> // back_inserter, istream_iterator
#include <sstream>  // istringstream
#include <string>   // string
#include <vector>   // vector

#include "Allocator.hpp"

using namespace std;

// ----
// allocate_read: reads in input and returns vector of ints
// ----

vector<int> allocate_read () {
    string s;
    getline(cin, s);

    vector<int> _v;

    while (!s.empty()) {

        _v.push_back(stoi(s));

        if (cin.eof())
            break;

        getline(cin, s);
    }

    return _v;
}

// ----
// allocate_eval: executes allocator functions and returns allocator
// ----

My_Allocator<double, 1000> allocate_eval (vector<int>& _v) {

    assert(!_v.empty());

    My_Allocator<double, 1000> _a;

    while (!_v.empty()) {
        int action = _v[0];
        _v.erase(_v.begin());

        if (action > 0) {
            double* block = _a.allocate(action);
            for (int i = 0; i < action; ++i) {
                _a.construct(block + i, 0);
            }

        } else {
            double* block = _a.find_busy_block(action);
            if (block) {
                _a.deallocate(block);
                _a.destroy(block);
            }
        }
    }

    return _a;
}

// ----
// allocate_print: prints state of heap
// ----

void allocate_print (My_Allocator<double, 1000>& _a) {

    My_Allocator<double, 1000>::iterator b = _a.begin();
    My_Allocator<double, 1000>::iterator e = --_a.end();

    while (b != e) {
        cout << *b << " ";
        ++b;
    }

    cout << *e << endl;

}


// ----
// main
// ----

int main () {
    /*
    the acceptance tests are hardwired to use My_Allocator<double, 1000>
    */

    int num_tests;
    cin >> num_tests;

    string s;
    getline(cin, s);
    getline(cin, s);

    for (int i = 0; i < num_tests; ++i) {
        vector<int> _v = allocate_read();
        My_Allocator<double, 1000> _a = allocate_eval(_v);
        allocate_print(_a);
    }

    return 0;
}
